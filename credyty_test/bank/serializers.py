from rest_framework import serializers
from bank.models import *

from bank.calculate_password import CalculatePassword


class AccountSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Account
        fields = ('__all__')

    def create(self, validated_data):
        account = Account.objects.create(**validated_data )
        account.save()

        rules_list = []
        with open(account.file.path, "r") as fd:
            rules_list = fd.read().splitlines()
        print(rules_list)
        fd.close()

        calculate = CalculatePassword(rules_list)
        passw = calculate.get_password()
        print(passw)
        account.password = passw   
        account.save()
        return  account
