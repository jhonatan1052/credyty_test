from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import viewsets,generics
from bank.models import *
from bank.serializers import *

# Create your views here.



class AccountViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


