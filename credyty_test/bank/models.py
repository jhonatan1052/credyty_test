from django.db import models
from django.db.models import *

# Create your models here.


class Account(models.Model):
    first_name = CharField(max_length=45, null = True)
    last_name = CharField(max_length=90 , null = True)
    file = FileField(upload_to='static/documents/')
    password = CharField(max_length=10 , null = True)
    