class CalculatePassword(object):
    # Autor : Jhonatan Acelas Arevalo

    sequence = []
    password = []

    def __init__(self,sequence):
        self.sequence = sequence
        self.password = list( self.sequence[0] ) 
        del self.sequence[0]


    def contains(self,list1, list2): 
        result = False
        for x in list1: 
            for y in list2: 
                if x == y: 
                    result = True
                    return result  
        return result 

    def next_append(self, rule):
        #Go through each character of the rule, and evaluate condition forward and back
        for r in rule:
            try:
                #Get the position of the character of the rule in the password
                index = self.password.index(r)
                #Obtains the position in front of the rule that exists in the password to evaluate if it exists or should be added
                pos_next =  rule.index(r) + 1
                if pos_next <= len(rule):   
                    has_num = False
                    #Evaluate if the next character that matches between the rule and the password exists before the password
                    for pos_upper_pass in range(index,len(self.password)):
                        if rule[pos_next]==self.password[pos_upper_pass]:
                            has_num= True
                    #if it does not exist, add it ahead of the character that matches the rule
                    if has_num==False:
                        #add forward
                        self.password.insert(index+1 ,rule[pos_next] )

                        #travels the password backwards, in case there is a delete
                        for back in range(0,index):
                            if rule[pos_next] ==self.password[back]:
                                del self.password[back]


                #get the position behind the rule that exists in the password to evaluate if it exists or should be added
                pos_back =  rule.index(r) - 1   

                if pos_back >=0:
                    has_num = False
                    #Evaluates if the previous character that matches between the rule and the password exists before the password
                    for pos_upper_pass in range(0,index):
                        if rule[pos_back]==self.password[pos_upper_pass]:
                            has_num= True
                    #if it does not exist, add it ahead of the character that matches the rule        
                    if has_num==False:
                        # add back 
                        self.password.insert(index-1 ,rule[pos_back] )
                        #Scroll the password forward, in case there is the delete
                        for back in range(index, len(self.password)):
                            if rule[pos_back] ==self.password[back]:
                                del self.password[back]
            except:
                #This character does not exist even in the password
                pass

    def get_password(self):
        for  item in self.sequence:
            rule = list(item)
            #check if the new rule to be added has a character in common with the password
            if  self.contains(self.password,rule) :
                #If it exists, send to add 
                self.next_append(rule)
            else:
                #if it does not exist, send a queue at the end to re-evaluate
                self.sequence.append(item)
        return self.password